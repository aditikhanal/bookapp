import React from 'react';
import AppSwitchNavigator from './navigator/appNavigator.js';

export default function App() {
  return(
    <AppSwitchNavigator/>
  )
}
