import {
    createAppContainer,
    createSwitchNavigator,
  } from 'react-navigation';
  import React, { Component } from 'react';
  import { View, TouchableOpacity, Image, Dimensions } from 'react-native';
  import Login from "../pages/LoginScreen.js";
  import Register from "../pages/RegisterScreen.js";
  import bottomTabNavigator from "./bottomNavigator.js";
  import {createStackNavigator} from "react-navigation-stack";
  import Intro1 from "../pages/Introscreen1.js";
  import Intro2 from "../pages/IntroScreen2.js";
  import Intro3 from "../pages/IntroScreen3.js";
  import {createDrawerNavigator} from 'react-navigation-drawer';
  import ContentComponent from './drawerComponent.js';
  import Loading from "../pages/Loading"
 



const DashboardTabNavigator = bottomTabNavigator

const DashboardStackNavigator = createStackNavigator({
  DashboardTabNavigator: {
    screen: DashboardTabNavigator,
   
   
    navigationOptions: {
      header: ()=> false
      

    }
  
      
    
      
    //   headerLeft: (
      
    //     <Image source={require('../images/Hamburger.png')} style={{ height: 15, width: 20, marginLeft: 25, marginTop: 31, tintColor:'black' }} />

    //   )
    
  }

},);

const AppDrawerNavigator = createDrawerNavigator({
  Dashboard: {
    screen: DashboardStackNavigator,
  },                           
}, {
  drawerBackgroundColor: 'green',   
  overlayColor: 'rgba(0, 0, 0, 0.6)',
  contentComponent: ContentComponent,



});


  const AppSwitchNavigator = createSwitchNavigator({
    Loading:{screen:Loading},
    Intro1:{screen:Intro1},
    Intro2:{screen:Intro2},
    Intro3:{screen:Intro3},
    Register:{screen:Register},
    Login:{screen:Login},
    Dashboard: {
      screen: AppDrawerNavigator
    },
})
  
  const App = createAppContainer(AppSwitchNavigator);
  export default App;