import Buy from "../pages/Tab2"
import Sell from "../pages/Exchange"
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

  import React, { Component } from 'react';
  import { View, TouchableOpacity, Image, Dimensions } from 'react-native';

const topTabNavigator2=createMaterialTopTabNavigator(
    {
      ExchangeBooks: {
        screen:Buy
        
      },
      ExchangeForm:{screen:Sell},
      
  },
  {
    initialRouteName: "ExchangeBooks",
    /* The header config from HomeScreen is now here */

    
    
    tabBarOptions: {
      //scrollEnabled:true,
      activeTintColor: '#7B1FA2',
     
      upperCaseLabel: false,
      activeColor:"#7B1FA2",
      inactiveTintColor: '#9b9b9b',
      labelStyle: {
        fontFamily: 'ProximaNova-Semibold',
        fontSize: 16,
        lineHeight: 20,
        fontWeight: '600',
      
        
        
      },
      indicatorStyle: {
        borderBottomColor: '#7B1FA2',
        borderBottomWidth: 3,
      },
      style: {
        backgroundColor: '#fff',
        borderRadius:8,
      },
    }
  }
  
  )
  export default topTabNavigator2;