import { createBottomTabNavigator } from 'react-navigation-tabs';
import Tab1 from '../pages/Tab1.js'
import Tab2 from '../pages/Tab2.js'
import Tab3 from "../pages/Tab3.js"
import {
    createAppContainer,
    createSwitchNavigator,
 
  } from 'react-navigation';
  import {createStackNavigator} from 'react-navigation-stack';
  import React, { Component } from 'react';
  import { View, TouchableOpacity, Image, Dimensions } from 'react-native';
  import topTabNavigator from "./topNavigator"
  import topTabNavigator2 from "./topNavigator2";
  import topTabNavigator3 from "./topNavigator3";
  import { withNavigation } from 'react-navigation';
  import { DrawerActions } from 'react-navigation-drawer';
  
  const DashboardStackNavigator4 = createStackNavigator({
    
    DashboardTabNavigator2: {
      screen: topTabNavigator3,
      navigationOptions : ({navigation}) =>({
      
        title: 'Best Sellers',
        headerStyle: {
          
              backgroundColor: '#7B1FA2',
            
              height: 63,
              
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'ProximaNova-Semibold',
          fontSize: 20,
         // lineHeight: 20,
          fontWeight: '600',
          color: '#fff',
          marginLeft: 50,
          marginRight: 74,
          marginBottom: 23,
          marginTop: 45
        },
        headerLeft:()=>  (
          <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
          <Image source={require('../assets/img/Hamburger.png')} style={{ height: 15, width: 20, marginLeft:20,marginTop:15}}  />
          </TouchableOpacity>
        ),
       
    }),
   

   
     }
  });

 
  const DashboardStackNavigator3 = createStackNavigator({
    DashboardTabNavigator2: {
      screen: topTabNavigator2,
      navigationOptions : ({navigation}) =>({
      
        title: 'Exchange Books',
        headerStyle: {
          
              backgroundColor: '#7B1FA2',
            
              height: 63,
              
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'ProximaNova-Semibold',
          fontSize: 20,
         // lineHeight: 20,
          fontWeight: '600',
          color: '#fff',
          marginLeft: 50,
          marginRight: 74,
          marginBottom: 23,
          marginTop: 45
        },
        headerLeft:()=>  (
          <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
          <Image source={require('../assets/img/Hamburger.png')} style={{ height: 15, width: 20, marginLeft:20,marginTop:15}}  />
          </TouchableOpacity>
        ),
       
    }),
   

   
     }
  });

  const DashboardStackNavigator2 = createStackNavigator({
    DashboardTabNavigator2: {
      screen: topTabNavigator,
      navigationOptions :({navigation}) =>( {
      
        title: 'Buy or Sell Books',
        headerStyle: {
          
              backgroundColor: '#7B1FA2',
            
              height: 63,
              
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontFamily: 'ProximaNova-Semibold',
          fontSize: 20,
         // lineHeight: 20,
          fontWeight: '600',
          color: '#fff',
          marginLeft: 50,
          marginRight: 74,
          marginBottom: 23,
          marginTop: 45
        },
        headerLeft:()=>  (
          <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
          <Image source={require('../assets/img/Hamburger.png')} style={{ height: 15, width: 20, marginLeft:20,marginTop:15}}  />
          </TouchableOpacity>
        ),
       
    })
   
     }
  });
const bottomTabNavigator = createBottomTabNavigator({
    Tab1: {
      screen: DashboardStackNavigator2,
      navigationOptions: {
        tabBarLabel: 'Sell/Buy',
        // tabBarIcon: ({ tintColor }) => (
        //   <View>
        //     <Image source={require('../assets/img/farmers.png')} style={{ tintColor: tintColor, height: 25, width: 25, marginRight: 5 }} />
        //   </View>
        // )
      }
    },
    Tab2: {
      screen:DashboardStackNavigator3,
      navigationOptions: {
        header: {
          visible: true,
        },
        tabBarLabel: 'Exchange',
        // tabBarIcon: ({ tintColor }) => (
        //   <View>
        //      <Image source={require('../assets/img/industry.png')} style={{ tintColor: tintColor, height: 25, width: 25, marginRight: 5 }} />
        //   </View>
        // )
       
          title: 'Login',
          headerStyle: {
            
                backgroundColor: '#388E3C',
              
                height: 63,
                
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontFamily: 'ProximaNova-Semibold',
            fontSize: 20,
           // lineHeight: 20,
            fontWeight: '600',
            color: '#fff',
            marginLeft: 100,
            marginRight: 74,
            marginBottom: 23,
            marginTop: 45
          },
      
        
          
      }
    },
    Tab3: {
      screen:DashboardStackNavigator4,
      navigationOptions: {
        tabBarLabel: 'Best sellers',
        // tabBarIcon: ({ tintColor }) => (
        //   <View>
        //      <Image source={require('../assets/img/upload.png')} style={{ tintColor: tintColor, height: 25, width: 25, marginRight: 5 }} /> 
        //   </View>
        // )
      }
    },
    
  
    
 
    })
  
  
export default withNavigation(bottomTabNavigator);
