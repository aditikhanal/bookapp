import Buy from "../pages/Tab1"
import Sell from "../pages/Sell"
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

  import React, { Component } from 'react';
  import { View, TouchableOpacity, Image, Dimensions } from 'react-native';

const topTabNavigator=createMaterialTopTabNavigator(
    {
      Buy: {
        screen:Buy
        
      },
      Sell:{screen:Sell},
      
  },
  {
    initialRouteName: "Buy",
    /* The header config from HomeScreen is now here */

    
    
    tabBarOptions: {
      // scrollEnabled:true,
      // animationEnabled: false,
      activeTintColor: '#7B1FA2',
     
      upperCaseLabel: false,
      activeColor:"#7B1FA2",
      inactiveTintColor: '#9b9b9b',
      labelStyle: {
        fontFamily: 'ProximaNova-Semibold',
        fontSize: 16,
        lineHeight: 20,
        fontWeight: '600',
      
        
        
      },
      indicatorStyle: {
        borderBottomColor: '#7B1FA2',
        borderBottomWidth: 3,
      },
      style: {
        backgroundColor: '#fff',
        borderRadius:8,
      },
    }
  }
  
  )
  export default topTabNavigator;