import React, { useState,Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    FlatList,
    KeyboardAvoidingView,
    ActivityIndicator,
    Alert
} from 'react-native'
import {
    TextField,
    FilledTextField,
    OutlinedTextField,
  } from 'react-native-material-textfield';
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/auth';


export default class Intro extends Component {
  state={email:'',password:''}
   register = async() => {
    const { email, password } = this.state

    
    try {
      
        const doRegister = await auth().createUserWithEmailAndPassword(email.trim(), password);
       
        if(doRegister.user) {
            this.props.navigation.navigate('Dashboard');
        }

    } catch (e) {
        auth().createUserWithEmailAndPassword(email.trim(), password)
        Alert.alert(
            e.message
        );
    }
};
validate(text, type) {
   
    email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
    password = /[a-zA-Z]+$/
    if (type == 'email') {
        if (email.test(text)) {
          this.setState({
            emailValidate: true
          })
        }
        else {
          this.setState({
            emailValidate: false
          })
        }
      }
      else if (type == 'password') {
        if (password.test(text)) {
          this.setState({
            passwordValidate: true
          })
        }
        else {
          this.setState({
            passwordValidate: false
          })
        }
      }   
}




    render() {
     return (
            <KeyboardAvoidingView
            //behavior="padding">
            >
            <ScrollView
       //  contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                   
                 <View>
                    
                <View >
                      <Image source={require('../assets/img/register.png')} style={styles.image} />  
                    
               
                  
                            <TextInput  
                            onChangeText={email => this.setState({ email})}
                            style={{width:335,
                                    height:40,
                                    backgroundColor:"#F3E5F5",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:30,
                                    borderRadius:8,
                                
                                    marginLeft:30}}
                                    placeholder="Enter your Email"
                                    autoCapitalize="none"
                                    ></TextInput>
                                    <TextInput 
                                         onChangeText= {password => this.setState({ password})} 
                            style={{width:335,
                                    height:40,
                                    backgroundColor:"#F3E5F5",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:10,
                                    borderRadius:8,
                                    marginLeft:30}}
                                    placeholder="Enter password"
                                    autoCapitalize="none"
                                    secureTextEntry={true}
                                    ></TextInput>
                                      
                    </View>
                    <TouchableOpacity  style={{marginTop:50,justifyContent:"center",marginLeft:75}}
                        onPress={() => this.props.navigation.navigate('Login')}
                        >
                       

                        <Text style={{color:"black", fontSize:16}}>
                            Already Registered? Click here to Login
                    </Text>
                            </TouchableOpacity>
                    

                    <TouchableOpacity style={styles.button}
                        onPress={this.register}>
                        

                        <Text style={styles.buttonText}>
                            Register
                    </Text>
                    </TouchableOpacity>
                   
                 </View> 
                 </ScrollView>
                 </KeyboardAvoidingView>
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {
   backgroundColor: '#E1BEE7',
    height: ScreenHeight,
  },
    image: {
       // width: 258,
        //height: 258,
        alignSelf: 'center',
        marginTop: 60
    },
    textTitle: {
        alignSelf: 'center',
        color: '#434343',
        fontSize: 22,
        fontWeight: '700',
        fontFamily: 'ProximaNova-Regular',
        marginTop:30
    },
    detail: {
        alignSelf: 'center',
        width: 296,
       // height: 88,
        marginTop: 10,
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '400',
        color: '#767676',
        lineHeight: 20,
        fontFamily:'ProximaNova-Semibold'

    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'ProximaNova-Regular',
        fontWeight: '700',


    },
    button: {
        backgroundColor: '#7B1FA2',
        width: 200,
        height: 56,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: 'center',
        marginVertical: 10,
        borderRadius:50,
        marginTop: 5
    },
    skip: {
        color: '#00b0f2',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '400',
        fontFamily: "ProximaNova-Regular",
        marginTop: 25
    }

})