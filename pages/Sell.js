import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    Picker

} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
}
from 'react-native-responsive-screen';

import {CheckBox} from "react-native-elements"





export default class Intro extends Component {
    constructor() {
        super();
  
        this.state = {
            text1:"",
          checked1: false,
          checked2: false,
          checked3: false,
          checked4: false,
          checked5: false,
          checked6: false,
          checked7: false,
          checked8: false,
          
          checked9: false,
        };
      }
  

    render() {
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                
                    
               
                    {/* <Image source={require('../images/emergency.png')} style={styles.image} /> */}
                    
                  <TextInput  onChangeText={text1 => this.setState({ text1})}
                            style={{width:wp("85%"),
                                    height:40,
                                    backgroundColor:"#fff",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:20,
                                    borderRadius:8,
                                    marginLeft:30}}
                                    placeholder="Enter your Book's Name"></TextInput>
                   <TextInput  onChangeText={text2 => this.setState({ text2})}
                            style={{width:wp("85%"),
                                    height:40,
                                    backgroundColor:"#fff",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:10,
                                    borderRadius:8,
                                    marginLeft:30}}
                                    placeholder="Enter Author's name"></TextInput>
                                                       <TextInput  onChangeText={text2 => this.setState({ text2})}
                            style={{width:wp("85%"),
                                    height:40,
                                    backgroundColor:"#fff",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:10,
                                    borderRadius:8,
                                    marginLeft:30}}
                                    placeholder="Enter Book's price"
                                    keyboardType='numeric'></TextInput>
                   
                                                      
                                    
        
        <View flexDirection="row">
        
        
        <Text style={styles.detail}>Enter the genre of book</Text>
          <Picker
                  selectedValue={this.state.language}
                  style={{height: 50, width: 150}}
                  onValueChange={(itemValue, itemIndex) =>
                  this.setState({language: itemValue})
  }>
  <Picker.Item label="Fiction"  value="Fiction" />
  <Picker.Item label="Adventure"  value="Adventure"/>
  <Picker.Item label="Romance"value="Romance" />
  </Picker>
</View>



                    

                    <TouchableOpacity style={styles.button}
                        onPress={() => this.props.navigation.navigate('Dashboard')}>
                        

                        <Text style={styles.buttonText}>
                            Sell
                    </Text>
                    </TouchableOpacity>
                
               
            </ScrollView>
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {
   backgroundColor: '#E1BEE7',
    height: ScreenHeight,
  },
    image: {
        width: 258,
        height: 258,
        alignSelf: 'center',
        marginTop: 100
    },
    textTitle: {
        //alignSelf: 'center',
        color: '#434343',
        fontSize: 22,
        fontWeight: '700',
        fontFamily: 'ProximaNova-Regular',
        marginTop:30
    },
    detail: {
        //alignSelf: 'center',
        //width: 296,
       // height: 88,
        marginLeft:40,
        marginTop: 18,
        //textAlign: 'center',
        fontSize: 14,
        fontWeight: '400',
        color: 'black',
        lineHeight: 20,
        fontFamily:'ProximaNova-Semibold'

    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'ProximaNova-Regular',
        fontWeight: '700',


    },
    button: {
        backgroundColor: '#7B1FA2',
        width: 200,
        height: 56,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: 'center',
        marginVertical: 10,
        borderRadius:50,
        marginTop: 10
    },
    skip: {
        color: '#00b0f2',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '400',
        fontFamily: "ProximaNova-Regular",
        marginTop: 25
    }

})