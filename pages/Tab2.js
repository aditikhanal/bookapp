import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    FlatList
} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
}
from 'react-native-responsive-screen';

import {Card} from "native-base";
import { widthPercentageToDP } from 'react-native-responsive-screen';


export default class Intro2 extends Component {
    render(){
        return(
            <ScrollView style={{backgroundColor: "#fff"}}>
                
             
                <Card style={{backgroundColor:"#E1BEE7",width:wp("62%"),height:300,alignSelf:"center",marginTop:20,borderColor:"green"}}>
                   
                    <Image source={require("../assets/img/book_imagr.jpg")} style={{width:210,height:150,marginTop:25,marginLeft:20}}></Image>
                    <View flexDirection="column" style={{justifyContent:"center"}}>
                    <Text style={{alignSelf: 'center',
        color: '#434343',
        fontSize: 16,
        fontWeight: '700',
        marginRight:20,
        marginLeft:wp("20%"),
        width:200,
        fontFamily: 'ProximaNova-Regular',
        marginTop:30}}>The Hunger Games</Text>
                    
        <TouchableOpacity onPress={() => this.props.navigation.navigate('cropsinfo')}
              style={{backgroundColor:'#7B1FA2',width:100,height:35,marginTop:10,marginLeft:wp("19%"),borderRadius:12,justifyContent:"center"}}>
                  <Text style={{
                               // fontFamily: 'ProximaNova-Regular',
                                fontSize: 14,
                               // lineHeight: 20,
                                fontWeight: '700',
                                color: '#fff',
                                marginLeft:10,
                                marginTop:10,
                               // marginLeft: 115,
                                marginTop: 0,
                                // justifyContent: 'center'
                  }}>View details</Text></TouchableOpacity> 
                  
                    
               
                    </View>
           
                </Card>
                <Card style={{backgroundColor:"#E1BEE7",width:wp("62%"),height:300,alignSelf:"center",marginTop:20,borderColor:"green"}}>
                   
                    <Image source={require("../assets/img/book_imagr.jpg")} style={{width:210,height:150,marginTop:25,marginLeft:20}}></Image>
                    <View flexDirection="column" style={{justifyContent:"center"}}>
                    <Text style={{alignSelf: 'center',
        color: '#434343',
        fontSize: 16,
        fontWeight: '700',
        marginRight:20,
        marginLeft:wp("20%"),
        width:200,
        fontFamily: 'ProximaNova-Regular',
        marginTop:30}}>The Hunger Games</Text>
                    
        <TouchableOpacity onPress={() => this.props.navigation.navigate('cropsinfo')}
              style={{backgroundColor:'#7B1FA2',width:100,height:35,marginTop:10,marginLeft:wp("19%"),borderRadius:12,justifyContent:"center"}}>
                  <Text style={{
                               // fontFamily: 'ProximaNova-Regular',
                                fontSize: 14,
                               // lineHeight: 20,
                                fontWeight: '700',
                                color: '#fff',
                                marginLeft:10,
                                marginTop:10,
                               // marginLeft: 115,
                                marginTop: 0,
                                // justifyContent: 'center'
                  }}>View details</Text></TouchableOpacity> 
                  
                    
               
                    </View>
           
                </Card>
                <Card style={{backgroundColor:"#E1BEE7",width:wp("62%"),height:300,alignSelf:"center",marginTop:20,borderColor:"green"}}>
                   
                    <Image source={require("../assets/img/book_imagr.jpg")} style={{width:210,height:150,marginTop:25,marginLeft:20}}></Image>
                    <View flexDirection="column" style={{justifyContent:"center"}}>
                    <Text style={{alignSelf: 'center',
        color: '#434343',
        fontSize: 16,
        fontWeight: '700',
        marginRight:20,
        marginLeft:wp("20%"),
        width:200,
        fontFamily: 'ProximaNova-Regular',
        marginTop:30}}>The Hunger Games</Text>
                    
        <TouchableOpacity onPress={() => this.props.navigation.navigate('cropsinfo')}
              style={{backgroundColor:'#7B1FA2',width:100,height:35,marginTop:10,marginLeft:wp("19%"),borderRadius:12,justifyContent:"center"}}>
                  <Text style={{
                               // fontFamily: 'ProximaNova-Regular',
                                fontSize: 14,
                               // lineHeight: 20,
                                fontWeight: '700',
                                color: '#fff',
                                marginLeft:10,
                                marginTop:10,
                               // marginLeft: 115,
                                marginTop: 0,
                                // justifyContent: 'center'
                  }}>View details</Text></TouchableOpacity> 
                  
                    
               
                    </View>
           
                </Card>
             
            
            
            
            
            </ScrollView>
  
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {
    
    height: ScreenHeight,
  },
    image: {
        width: 258,
        height: 258,
        alignSelf: 'center',
        marginTop: 100
    },
    textTitle: {
        alignSelf: 'center',
        color: '#434343',
        fontSize: 22,
        fontWeight: '700',
        fontFamily: 'ProximaNova-Regular',
        marginTop:30
    },
    detail: {
        alignSelf: 'center',
        width: 296,
        height: 88,
        marginTop: 10,
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '400',
        color: '#767676',
        lineHeight: 20,
        fontFamily:'ProximaNova-Regular'

    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'ProximaNova-Regular',
        fontWeight: '700',


    },
    button: {
        backgroundColor: '#7B1FA2',
        width: 335,
        height: 56,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: 'center',
        marginVertical: 10,
        borderRadius: 5,
        marginTop: 60
    },
    skip: {
        color: '#00b0f2',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '400',
        fontFamily: "ProximaNova-Regular",
        marginTop: 25
    }

})