import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
}
from 'react-native-responsive-screen';

export default class Intro extends Component {

    render() {
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                <View style={{}} >
                     <Image source={require('../assets/img/exchange.png')} style={styles.image} /> 
                    
                    <Text style={styles.detail}>Exchange your books with the book of your choice
                </Text>
               
                    

                    <TouchableOpacity style={styles.button}
                        onPress={() => this.props.navigation.navigate('Register')}>
                        

                        <Text style={styles.buttonText}>
                            Next
                    </Text>
                    </TouchableOpacity>
                
                </View>
            </ScrollView>
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {
   // backgroundColor: '#00B0F2',
    height: ScreenHeight,
    backgroundColor: '#E1BEE7'
  },
    image: {
        width: 258,
        height: 258,
        alignSelf: 'center',
        marginTop: 100
    },
    textTitle: {
        alignSelf: 'center',
        color: '#434343',
        fontSize: 22,
        fontWeight: '700',
        fontFamily: 'ProximaNova-Semibold',
        marginTop:30
    },
    detail: {
        alignSelf: 'center',
        width: 296,
        height: 88,
        marginTop: 30,
        textAlign: 'center',
        fontSize: 24,
        fontWeight: '400',
        color: '#7B1FA2',
        lineHeight: 25,
        fontFamily:'ProximaNova-Semibold'

    },
    detail2: {
        alignSelf: 'center',
        width: 296,
        height: 88,
        marginTop: -20,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: '400',
        color: '#388E3C',
        //lineHeight: 20,
        fontFamily:'ProximaNova-Regular'

    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'ProximaNova-Regular',
        fontWeight: '700',


    },
    button: {
        backgroundColor: '#7B1FA2',
        width: 235,
        height: 56,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: 'center',
        marginVertical: 10,
        borderRadius: 20,
        marginTop: 60
    },
    skip: {
        color: '#00b0f2',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '400',
        fontFamily: "ProximaNova-Regular",
        marginTop: 25
    }

})