import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    KeyboardAvoidingView,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    TextInput,
    FlatList,
    Alert
} from 'react-native'
import auth from '@react-native-firebase/auth';
import { firebase } from '@react-native-firebase/auth';


export default class Intro extends Component {
    state={email:'',password:''}
    login = async() => {
        const { email, password } = this.state
    
        try {
            const doLogin = await auth().signInWithEmailAndPassword(email.trim(), password);
        
            if(doLogin.user) {
               this.props.navigation.navigate('Home');
            }
        } catch (e) {
   
            Alert.alert(
                e.message
            );
        }
    };

    render() {
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                   
                 <View>
                    
                <View >
                      <Image source={require('../assets/img/login.jpg')} style={styles.image} />  
                    
                 
                  
                            <TextInput  onChangeText={email => this.setState({email})}
                            style={{width:335,
                                    height:40,
                                    backgroundColor:"#F3E5F5",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:30,
                                    borderRadius:8,
                                
                                    marginLeft:30}}
                                    placeholder="Enter your Email"
                                    autoCapitalize="none"
                                    ></TextInput>
                                    <TextInput  onChangeText={password => this.setState({password})}
                            style={{width:335,
                                    height:40,
                                    backgroundColor:"#F3E5F5",
                                    borderColor:'gray',
                                    borderWidth:0.5,
                                    marginTop:10,
                                    borderRadius:8,
                                    marginLeft:30}}
                                    placeholder="Enter password"
                                    autoCapitalize="none"
                                    secureTextEntry={true}
                                    ></TextInput>
                                      
                    </View>
                    <TouchableOpacity  style={{marginTop:50,justifyContent:"center",marginLeft:75}}
                        onPress={() => this.props.navigation.navigate('Register')}>
                       

                        <Text style={{color:"black", fontSize:16}}>
                            New here? Click here to Register
                    </Text>
                            </TouchableOpacity>
                    

                    <TouchableOpacity style={styles.button}
                        onPress={this.login}>
                        

                        <Text style={styles.buttonText}>
                            Login
                    </Text>
                    </TouchableOpacity>
                
                 </View> 
                 </ScrollView>
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {
   backgroundColor: '#E1BEE7',
    height: ScreenHeight,
  },
    image: {
       // width: 258,
        //height: 258,
        alignSelf: 'center',
        marginTop: 60
    },
    textTitle: {
        alignSelf: 'center',
        color: '#434343',
        fontSize: 22,
        fontWeight: '700',
        fontFamily: 'ProximaNova-Regular',
        marginTop:30
    },
    detail: {
        alignSelf: 'center',
        width: 296,
       // height: 88,
        marginTop: 10,
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '400',
        color: '#767676',
        lineHeight: 20,
        fontFamily:'ProximaNova-Semibold'

    },
    buttonText: {
        fontSize: 20,
        color: 'white',
        fontFamily: 'ProximaNova-Regular',
        fontWeight: '700',


    },
    button: {
        backgroundColor: '#7B1FA2',
        width: 200,
        height: 56,
        alignSelf: 'center',
        alignItems: "center",
        justifyContent: 'center',
        marginVertical: 10,
        borderRadius:50,
        marginTop: 5
    },
    skip: {
        color: '#00b0f2',
        alignSelf: 'center',
        fontSize: 16,
        fontWeight: '400',
        fontFamily: "ProximaNova-Regular",
        marginTop: 25
    }

})